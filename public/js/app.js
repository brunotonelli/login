'use strict';

/**********************************************************************
 * Angular Application
 **********************************************************************/
var app = angular.module('app', ['ngResource', 'ngRoute'])
  .config(function($routeProvider, $locationProvider, $httpProvider) {
    //================================================
    // Check if the user is connected
    //================================================
    var checkLoggedin = function($q, $timeout, $http, $location, $rootScope){
      // Initialize a new promise
      var deferred = $q.defer();

      // Make an AJAX call to check if the user is logged in
      $http.get('/loggedin').success(function(user){
        // Authenticated
        if (user !== '0')
          /*$timeout(deferred.resolve, 0);*/
          deferred.resolve();

        // Not Authenticated
        else {
          $rootScope.message = 'You need to log in.';
          //$timeout(function(){deferred.reject();}, 0);
          deferred.reject();
          $location.url('/login');
        }
      });

      return deferred.promise;
    };
    //================================================
    
    //================================================
    // Add an interceptor for AJAX errors
    //================================================
    $httpProvider.interceptors.push(function($q, $location) {
      return {
        response: function(response) {
          // do something on success
          return response;
        },
        responseError: function(response) {
          if (response.status === 401)
            $location.url('/login');
          return $q.reject(response);
        }
      };
    });
    //================================================

    //================================================
    // Define all the routes
    //================================================
    $routeProvider
      .when('/', {
        templateUrl: '/views/main.html'
      })
      .when('/admin', {
        templateUrl: 'views/admin.html',
        controller: 'AdminCtrl',
        resolve: {
          loggedin: checkLoggedin
        }
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })

      .otherwise({
        redirectTo: '/'
      });
    //================================================

  }) // end of config()
  .run(function($rootScope, $http){
    $rootScope.message = '';

    // Logout function is available in any pages
    $rootScope.logout = function(){
      $rootScope.message = 'Logged out.';
      $http.post('/logout');
    };
  });


/**********************************************************************
 * Login controller
 **********************************************************************/
app.controller('LoginCtrl', function($scope, $rootScope, $http, $location) {
    // This object will be filled by the form
    $scope.user = {};

    // Register the login() function
    $scope.login = function(){
        $http.post('/login', {
            username: $scope.logUsername,
            password: $scope.logPassword
        })
            .success(function(user){
                // No error: authentication OK
                $rootScope.message = 'Authentication successful!';
                $location.url('/admin');
            })
            .error(function(){
                // Error: authentication failed
                $rootScope.message = 'Authentication failed.';
                $location.url('/login');
            });}

    $scope.registrar = function() {
        $http.post('/registrar', {
            username: $scope.username,
            password: $scope.password,
            email: $scope.email,
            nombre: $scope.nombre,
            edad: $scope.edad,
            permisos: $scope.permisos
        })
            .success(function(user){
                $rootScope.message = 'Authentication successful!';
                $location.url('/admin');
            })
            .error(function(){
                $rootScope.message = 'Authentication failed.';
                $location.url('/login');
            });}
});



/**********************************************************************
 * Admin controller
 **********************************************************************/
app.controller('AdminCtrl', function($scope, $http) {
  // List of users got from the server

  $scope.usuarios= [];

    // Fill the array to display it in the page
    $http.get('/usuarios').success(function(usuarios){
        for (var i in usuarios)
            $scope.usuarios.push(usuarios[i]);
    });


    $scope.llenarInfo = function(usuario){

        //paso de parametro el usuario del link
        $('#infoID').text(usuario._id);
        $('#infoUsername').text(usuario.username);
        $('#infoPassword').text(usuario.password);
        $('#infoEmail').text(usuario.email);
        $('#infoNombre').text(usuario.nombre);
        $('#infoEdad').text(usuario.edad);
        $('#infoPermisos').text(usuario.permisos);

        $('#divEditar').hide(); //escondo el div editar
        $('#divInfo').show(); //muestro el div con la info

    };
    $scope.agregar = function() {

        var todosLlenos = true;
        $("#divAgregar input").each(function() {
            if ($(this).val() == "") {
                error = false;
            }
        });
        //checkeo que los input no sean null
        if (todosLlenos) {
            $http.post('/usuarios/agregar', {
                username: $scope.addUsername,
                password: $scope.addPassword,
                email: $scope.addEmail,
                nombre: $scope.addNombre,
                edad: $scope.addEdad,
                permisos: $scope.addPermisos
            });
            alert("Usuario creado con exito!");
        }
        else alert("Por favor llene todos los campos");
    };

    $scope.llenarEditar = function(usuario){
        //le paso de parametro el usuario del link editar
        //lleno los input con los datos
        $scope.editID = usuario._id;
        $scope.editUsername = usuario.username;
        $scope.editPassword = usuario.password;
        $scope.editEmail = usuario.email;
        $scope.editNombre = usuario.nombre;
        $scope.editEdad = usuario.edad;
        $scope.editPermisos = usuario.permisos;

        $('#divInfo').hide(); //escondo el div info
        $('#divEditar').show(); //muestro el div con los input para editar
    };

    $scope.guardarCambios = function() {

        var todosLlenos = true;
        $("#divEditar input").each(function() {
            if ($(this).val() == "") {
                error = false;
            }
        });
        //checkeo que los input no sean null
        if (todosLlenos) {

            $http.post('/usuarios/editar', {
                editarID: $scope.editID,
                username: $scope.editUsername,
                password: $scope.editPassword,
                email: $scope.editEmail,
                nombre: $scope.editNombre,
                edad: $scope.editEdad,
                permisos: $scope.editPermisos
            });
            alert("Usuario editado con exito!");
            $('#divEditar').hide();
        }
        else alert("Por favor llene todos los campos");
    };

    $scope.borrar = function(borrarID) {
        $http.post('/usuarios/borrar', {
            borrarID: borrarID
        });
        alert("Usuario eliminado con exito!");
    };

});