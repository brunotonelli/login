var express = require('express');
var http = require('http');
var path = require('path');
var passport = require('passport');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var LocalStrategy = require('passport-local').Strategy;
var passportLocalMongoose = require('passport-local-mongoose');

mongoose.connect('mongodb://localhost/login-tonelli');


var Cuenta = new Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  nombre: {
    type: String,
    required: true
  },
  edad: {
    type: Number,
    required: true
  },
  permisos:{
    type: String,
    required: true
  }
});

Cuenta.plugin(passportLocalMongoose);

var Usuario = mongoose.model('Usuario', Cuenta);

passport.use(new LocalStrategy(
  function(username, password, done) {

    Usuario.findOne( {username: username}, function(err, obj) {

      if (obj!=null) {
        if (password == obj.password) {
          return done(null, {name: username});
        }
        else
          return done(null, false, { message: 'Usuario no valido' });
      }
    });
  }
));

// Serialized and deserialized methods when got from session
passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

// Define a middleware function to be used for every secured routes
var auth = function(req, res, next){
  if (!req.isAuthenticated())
  	res.send(401);
  else
  	next();
};
//==================================================================

// Start express application
var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser()); 
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.session({ secret: 'securedsession' }));
app.use(passport.initialize()); // Add passport initialization
app.use(passport.session());    // Add passport initialization
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// Configuring Passport
var expressSession = require('express-session');
app.use(expressSession({secret: 'mySecretKey'}));
app.use(passport.initialize());
app.use(passport.session());

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


//========================================
//POSTS PARA EDITAR TABLA

app.post('/usuarios/agregar', function(req, res){

  //igual que registrar
  var newUsuario = new Usuario({
    username : req.body.username,
    password: req.body.password,
    email: req.body.email,
    nombre: req.body.nombre,
    edad: req.body.edad,
    permisos: req.body.permisos
  });
  newUsuario.save();
  console.log("Usuario "+newUsuario.username+" creado exitosamente!");

});

app.post('/usuarios/editar', function(req,res){
  //updateo el usuario con la id q me manda el post

  Usuario.findOne({ _id: req.body.editarID }, function (err, usuario){
    usuario.username = req.body.username;
    usuario.password = req.body.password;
    usuario.email = req.body.email;
    usuario.nombre = req.body.nombre;
    usuario.edad = req.body.edad;
    usuario.permisos = req.body.permisos;
    usuario.save();
    console.log("Usuario "+usuario.username+ " editado con exito!");
  });
});

app.post('/usuarios/borrar', function(req, res){
  Usuario.findById(req.body.borrarID, function(err, usuario){
    Usuario.remove({_id : usuario._id}, function(err,removed) {
      console.log("Usuario "+usuario.username+" eliminado exitosamente!");
    });
  });
});

//get para pedir users y llenar tabla
app.get('/usuarios', auth, function(req, res){
  //mando todos los usuarios en la db
  Usuario.find({}, function (err, usuarios){
    res.send(usuarios);
  });
});

// route to test if the user is logged in or not
app.get('/loggedin', function(req, res) {
  res.send(req.isAuthenticated() ? req.user : '0');
});

// route to log in
app.post('/login', passport.authenticate('local'), function(req, res) {
  res.send(req.body);
});

app.post('/registrar', function(req, res) {
  var newUsuario = new Usuario({
    username : req.body.username,
    password: req.body.password,
    email: req.body.email,
    nombre: req.body.nombre,
    edad: req.body.edad,
    permisos: req.body.permisos
  });
  newUsuario.save();
});

// route to log out
app.post('/logout', function(req, res){
  req.logOut();
  res.send(200);
});
//==================================================================

app.listen(3000);